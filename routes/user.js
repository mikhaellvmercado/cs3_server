const express = require('express'); 
const router =  express.Router(); 
const UserController = require('../controllers/user');
const auth = require('../auth')

//[Primary Routes]
//register new user
router.post('/register', (req, res) => {
	UserController.register(req.body).then(result => res.send(result));
})

//checking for duplicate emails
router.post('/email-exists', (req, res) => {
    UserController.emailExists(req.body).then(result => res.send(result)); 
})

//login user
router.post('/login', (req, res) => {
    UserController.login(req.body).then(result => res.send(result));
})


//add category
//before being allowed to add a category in the db the user has to be authenticated by the app first
router.post('/add-category', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.addCategory(req.body).then(result => res.send(result));
})

router.get('/details', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization)
    UserController.get({ userId: user.id }).then(user => res.send(user))
})

router.post('/get-categories', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.getCategories(req.body).then(result => res.send(result));
})

router.post('/add-record', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authorization).id
    UserController.addRecord(req.body).then(result => res.send(result))
})

router.post('/get-records-breakdown-by-range', auth.verify, (req, res) => {
    req.body.userId = auth.decode(req.headers.authrization).id
    UserController.getRecordsBreakdownByRange(req.body).then(result => res.send(result));
})

//[SECONDARY ROUTES] 

module.exports = router