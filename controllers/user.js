const User = require('../models/user')
const bcrypt = require('bcrypt')
const auth = require('../auth'); 
const moment = require('moment');
//we will build all logic and process that involves our users

//[Primary Sections]

//check if the email already exists
module.exports.emailExists = (params) => {
   return User.find({ email: params.email }).then(result => {
    	return result.length > 0 ? true : false
    })
}

module.exports.register = (params) => {
   let newUser = new User({
   	firstName: params.firstName,
   	lastName: params.lastName,
   	email: params.email,
   	mobileNo: params.mobileNo,
   	password: bcrypt.hashSync(params.password, 10)
   })

   return newUser.save().then((user, err) => {
       return (err) ? false : true //ternary format
   })
}

//lets now create our login function 
module.exports.login = (params) => {
    console.log(params.email) //checking purposes
    const {email, password} = params
    return User.findOne({email}).then( user => {
    	//what if there are no records found?
    	if(!user) return false;
    	//compare the password to hashed password in db
    	let isPasswordMatched = bcrypt.compareSync(password, user.password)
    	if(!isPasswordMatched) return false; 

    	//and if the passwords do match, then generate an access token.
    	let accessToken = auth.createAccessToken(user)
    	return {
    		accessToken: accessToken
    	}
    })
} 

module.exports.get = (params) => {
    return User.findById(params.userId).then(user => {
        return { email: user.email }
    })
}

//create a function to add new category
module.exports.addCategory = (params) => {
    //the app needs to identify the user first
    return User.findById(params.userId).then(user => {
       user.categories.push({
       	   name: params.name,
       	   type: params.typeName  
       })
      
      return user.save().then((user, err) => {
         return (err) ? false : true
      })
    })
}

module.exports.getCategories = (params) => {
    
    return User.findById(params.userId).then(user => {

        if(typeof params.name === "undefined"){
            return user.categories
        }
        return user.categories.filter((category) => {
            if(category.type === params.typeName){
                return category
            }
        })
    })
}

module.exports.addRecord = (params) => {
    return User.findById(params.userId).then(user => {
        let balanceAfterTransaction = 0

        if(user.transactions.length !== 0) {
            const balanceBeforeTransaction = user.transactions[user.transactions.length -1].balanceAfterTransaction
        if(params.typeName === 'Income'){
           balanceAfterTransaction = balanceBeforeTransaction + params.amount
        }else{
           balanceAfterTransaction = balanceBeforeTransaction - params.amount
        }
        }else{
            balanceAfterTransaction = params.amount

        }

        user.transactions.push({
            categoryName: params.categoryName,
            type: params.typeName,
            amount: params.amount,
            description: params.description,
            balanceAfterTransaction: balanceAfterTransaction
        })

        return user.save().then((user, err) => {
            return (err) ? false : true
        })
    })
}

module.exports.getRecordsBreakdownByRange = (params) => {
    return User.findById(params.userId).then(user => {
        user.categories.map((category) => {
            return { categoryName: category.name, totalAmount: 0 }
        })
        user.transactions.filter((transaction) => {
            const isSameOrAfter = moment(transaction.dateAdded).isSameOrAfter(params.fromDate, 'day')
            const isSameOrBefore = moment(transaction.dateAdded).isSameOrBefore(params.toDate, 'day')

            if(isSameOrAfter && isSameOrBefore) {
                for(let i = 0; i < summary.length; i++) {
                    if(summary[i].categoryName === transaction.categoryName) {
                        summary[i].totalAmount += transaction.amount
                    }
                }
            }
        })
        return summary
    })
}

//[Secondary Functions]