const mongoose = require('mongoose'); 

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, 'firstName is Required!']//add a required key
	},
	lastName: {
		type: String,
		required:[true, 'lastName is Required!']
	},
	email: {
		type: String, 
		required: [true, 'Email is Required!']
	},
	password: {
		type: String, 
		required: [true, 'Password is Required!']
	},
	mobileNo: {
		type: String,
		required: [true, 'mobileNo is Required!']
	},
	categories: [
 	   {
 	   	 name: {
 	   	 	type: String,
 	   	 	required: [true, 'Category Name is required']
 	   	 },
 	   	 type: {
 	   	 	type: String,
 	   	 	required: [true, 'Category type is required']
 	   	 }
 	   }
	],
	transactions: [
      {
      	categoryName: {
      		type: String,
      		required: [true, 'Category Name is required']
      	},
      	type: {
      		type: String,
      		required: [true, 'Category Type is required']
      	},
      	amount: {
      		type: Number,
      		required: [true, 'Amount is required']
      	},
      	description: {
      		type: String,
      		default: null
      	},
      	balanceAfterTransaction: {
      		type: Number,
      		required: [true, 'Balance is required']
      	},
      	dateAdded: {
      		type: Date,
      		default: new Date()
      	}
      }
	]
})

module.exports = mongoose.model('user', userSchema)