const express = require('express');
const mongoose = require('mongoose'); 
const app = express();
const userRoutes = require('./routes/user'); 
const cors = require('cors');


mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas!"))
mongoose.connect(`mongodb+srv://MVMDB:goodgod100@cluster0.ugrca.mongodb.net/budget_tracker_server?retryWrites=true&w=majority`, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
//ill be giving you the choice if you want to utilize an env module.
// i suggest utilize an env module.

app.use(express.json())
app.use(express.urlencoded({ extended: true }))

//lets make a message to make sure that the project is properly hosted in heroku

app.get('/', (req, res) => {
    //were just going to display a message.
    console.log("successfully hosted onlin3");
    res.send("successfully hosted online");
})

app.use(cors())

app.use('/api/users', userRoutes)

//create an app listener and make sure that this backend project is running on port 4000
app.listen(process.env.PORT || 4000, () => {
	console.log("API is now up and running!")
})

